import {Component, OnInit} from '@angular/core';
import {RouletteService} from './shared/roulette.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  numbers: number[] = [];
  balance = 100;

  constructor(private rouletteService: RouletteService) {}

  ngOnInit() {
    this.rouletteService.newNumber.subscribe((numbers:number[]) => {
      this.numbers = numbers;
    });
  }

  getNumber() {
    return this.numbers.slice();
  }

  getStart() {
    this.rouletteService.start();
  }

  getStop() {
    this.rouletteService.stop();
  }

  getReset() {
    this.getStop();
    this.numbers = [];
  }
}
