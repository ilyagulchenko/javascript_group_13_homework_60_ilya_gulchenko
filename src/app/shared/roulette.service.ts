import {EventEmitter} from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<Number[]>();
  interval!: any;

  numbers: number[] = [];

  start() {
    this.interval = setInterval(() => this.generateNumber(), 1000);
  }

  generateNumber() {
    this.numbers.push(Math.floor(Math.random() * 37))
    this.newNumber.emit(this.numbers);
  }

  stop() {
    clearInterval(this.interval);
  }

  getColor(number: number) {
    if (number % 2 == 0 && number >= 1 && number <= 10) {
      return 'black';
    } else if (number % 2 !== 0 && number >= 1 && number <= 10) {
      return 'red';
    } else {
      return 'undefined';
    }
  }
}
