import { Directive, ElementRef, Input } from '@angular/core';
import { RouletteService } from './roulette.service';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {
  constructor(private el: ElementRef,private rouletteService: RouletteService) {
    rouletteService.getColor(this.appColor);
  }

  @Input() addClass () {
    this.el.nativeElement.classList.add(this.appColor);
  }

  @Input() appColor = 0;


}
